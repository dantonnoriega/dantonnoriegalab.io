---
title: Resume
---

::: {.d-flex .gap-2 .mb-3}
<a href="https://gitlab.com/dantonnoriega/dantonnoriega.gitlab.io/-/raw/main/resume/danton-noriega-resume-single_2024.pdf?inline=true" type="button" class="btn btn-primary"><i class="bi bi-file-earmark-pdf-fill"></i> View </a>

<a href="https://gitlab.com/dantonnoriega/dantonnoriega.gitlab.io/-/raw/main/resume/danton-noriega-resume-single_2024.pdf?inline=false" role="button" class="btn btn-info"><i class="bi bi-cloud-download-fill"></i> Download </a>
:::

------------------------------------------------------------------------

### EXPERIENCE

#### **Apple** | Cupertino, CA *(Current)*

-   **Staff Data Scientist** *(Oct 2023 - Present)*

    Cloud Data Engineering & Efficiency (iDEE) — May 2024 to Present
    
    - Predict demand for compute, storage, and network resources, with a primary focus on GPU infrastructure
    - Identify and implement platform optimizations within iCloud and Services to reduce costs, avoid unnecessary expenses, and efficiently manage emerging AI platforms

    Cloud Infrastructure Business Operations (CIBO) — Oct 2023 to May 2024

    - Measured baseline demand for Apple-owned infrastructure across all major organizations
    - Collaborated with business analysts to accurately capture and communicate expected demand to CIBO leadership
    - Developed a broad, high-level understanding of demand across the organization

-   **Senior Data Science Manager** *(Oct 2021 - Sep 2023)*
    - Led a team of 4 data scientists and 5 contractors in measuring and describing demand for and cost of Apple cloud infrastructure
    - Planned and managed team projects, advocated for direct reports, mentored junior data scientists, and recruited new talent
    - Proposed data strategies, adopted new technologies, built data products, and communicated results to leadership
    - Served as technical lead, writing R packages, reviewing code, and advising on statistical modeling, forecasting, and data challenges
    - Promoted better documentation, reproducibility, language agnosticism, and community building
-   **Data Science Manager** *(Oct 2020 - Sept 2021)*
    - Managed a small team of data scientists that partnered with engineering, services, and finance teams to analyze and model demand for, and the cost of, Apple-owned infrastructure.
-   **Senior Data Scientist** *(Oct 2018 - Sept 2020)*
    - Modeled blob and fast storage growth across Apple-owned data centers for Finance and Services
    - Deployed forecasts, Shiny dashboards, and dynamic reports into production using internal CI/CD tools and data science platform
    - Developed 10+ internal R packages to streamline data retrieval, automate workflows, and generate dynamic reports and presentations
    - Actively recruited at conferences and on social media, helping grow the team from 1 to 7 data scientists
-   **Data Scientist** *(Nov 2017 - Sept 2018)*
    - Modeled global demand for iCloud services (Photos, Backup, Drive) and its potential impact on Apple’s infrastructure for the Finance organization.
      
#### **Transloc** | Durham, NC *(2016)*

-   **Data Scientist** *(Sept 2016 - Dec 2016)*
    -   Continued working part-time, refining the multitudes of metrics available to the product team into understandable, actionable information exclusively in R.
-   **Data Science Intern** *(May 2016 - Aug 2016)*
    -   Developed products and packages that simplify working with and understanding transit data. Also learned/experimented with implementing the "agile" development philosophy but for data science.
    
    
#### **Duke University** | Durham, NC *(2015 - 2017)*

-   **Research Assistant**, Durham Children's Data Center *(Jan 2017 -- May 2017)*
    -   Analyzed data from Durham County Social Services for DCDC for Dr. Ken Dodge, Dr. Beth Gifford, and Dr. Anna Gassman-Pines.
-   **Research Assistant**, Duke-UNC BECR Center *(Jan 2015 -- May 2016)*
    -   Investigated food purchasing behavior, food-related health outcomes, and food assistance policy (SNAP/WIC). The aim was to uncover trends that can help BECR design behavioral nudges to improve food choices. Also helped write papers/proposals and providing analytical results (graphics, tables, etc).
-   **Teaching Assistant**, Sanford School of Public Policy *(Spring 2015)*
    -   Co-taught [*PubPol 590: Applied Big Data Science Energy Data Analytics and Policy*](https://github.com/dantonnoriega/PubPol590-Sp15) with Dr. Matthew Harding. Students learned introductory theory about causal inference and how to do data analysis with large data sets using Python. I designed all the homework assignments, projects, and exams. The goal was for students to finish the class with the capacity to do basic consulting for energy companies.

------------------------------------------------------------------------

### EDUCATION

-   **Ph.D. Public Policy & Economics** -- *Aug 2019*\
    *Duke University, Sanford School of Public Policy* (Durham, NC)
-   **M.A. Economics** -- *Feb 2013*\
    *Georgetown University* (Washington, DC)
-   **M.S. Applied Statistics** -- *May 2011*\
    *California State University at Long Beach* (Long Beach, CA)
-   **B.S. Applied & Computational Mathematics** -- *Aug 2006*\
    *University of California at Irvine* (Irvine, CA)
    *Orange Coast Community College* (Costa Mesa, CA)

------------------------------------------------------------------------

### PROGRAMMING

-   **R** -- Expert. 10+ years of experience. Preferred language. Love package development and data.table.
-   **SQL** -- Expert. 8+ years. Avid reader of PostgreSQL and HiveQL docs.
-   **bash** -- Expert user, proficient developer. 8+ years. Primarily for processing and exploration of raw text files.
-   **Python** -- Comfortable. 7+ years. Mostly for ELT. Taught graduate econometrics & causal inference in Python.
-   **Others** -- git, dbt, docker. Dabbled with Julia, Stan, and Scala. Amateur data engineer.

------------------------------------------------------------------------

### PACKAGES

-   [gtfsr](https://github.com/ropenscilabs/gtfsr/) -- for mapping and validation GTFS data.

------------------------------------------------------------------------

### DISSERTATION

-   *"Doubled SNAP Dollars and Nudges: An Analysis of Two Pilot Programs Aimed at Increasing the Purchase of Healthy Foods"*
    -   My dissertation is an analysis of two different pilot programs which aimed to increase the purchase of healthy foods, specifically produce. The first pilot program was a financial incentive known as "Double Up Food Bucks". The program targeted SNAP participants, encouraging them to purchase more fresh produce by effectively doubling purchasing power. The second pilot program was a set of three behavioral nudges designed to increase the purchase of bananas in a convenience store environment.

------------------------------------------------------------------------

### AWARDS AND HONORS

-   **Merit Based Fellowship (Economics)**, Georgetown University *(2011 -- 2015)*
-   **2010 STIPDG Outstanding Intern Award**, US Department of Transportation *(Summer 2010)*
-   **Fletcher Jones Fellowship**, University of California at Irvine (Awarded but chose not to pursue Ph.D.) *(Fall 2006)*
-   **Dean's Honor List**, University of California at Irvine *(Winter 2005 -- Spring 2006)*
-   **Early Transfer (Academic Excellence)**, Orange Coast College to UCI *(Winter 2005)*
-   **President's List for Academic Excellence**, Orange Coast College *(Fall 2003 -- Fall 2004)*
-   **Community College Scholarship Recipient**, Hispanic Education Endowment Fund (HEEF) *(Fall 2004)*

------------------------------------------------------------------------

### FUN FACTS

-   Dual citizen of Chile and the US
-   Native Spanish-speaker.
-   Avid Ultimate Frisbee player.
-   Former AmeriCorps NCCC Team Leader.
-   Cooks a mean kimchi & onion frittata.
