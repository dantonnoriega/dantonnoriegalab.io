# HOW TO RUN

## install basictex and key packages
brew install basictex
sudo tlmgr install kpfonts fontawesome enumitem

## build with pdflatex
pdflatex danton-noriega-resume-single_2024.TEX
